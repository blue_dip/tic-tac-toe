using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TicTactoe
{
    public partial class Form1 : Form
    {
        Player currentPlayer; //create a new instance of the player 'class'(enum) Deciding who is currently playing
        public Form1()
        {
            InitializeComponent();
        }

        public enum Player // alternating values representing alternating player, TODO: learn more about enum
        {
            X,
            O
        }
        private void Form1_Load(object sender, EventArgs e) //unclear what this is for, it is not in the tutorial
        {

        }

        private void buttonClick(object sender, EventArgs e)
        {
            var button = (Button)sender;
            currentPlayer = Player.X; //Player has X symbol? Why tho? Shouldn't it alternate with the symbol of the current player? -no because AI has O
            button.Text = currentPlayer.ToString();
            button.Enabled = false; // turns off the button, so it can not be clicked again
            button.BackColor = System.Drawing.Color.LightGreen; // the color of the button is changed to light green
            Check2in1(currentPlayer.ToString());
            AITimer.Start(); //start the timer for the AI (1second)
        }


        private void playAI(object sender, EventArgs e) //AI is real dumb right now, can we at least force it to put in square surrounded by least X:s or sth?
        {
            foreach (Control x in this.Controls) // loop over all the controls available
            {
                if (x is Button && x.Text == "?" && x.Enabled) // find buttons which have the text '?'
                {
                    x.Enabled = false; // disable the button so it cannot be clicked
                    currentPlayer = Player.O; // set the current player to O
                    x.Text = currentPlayer.ToString(); // write currentPlayer text to button
                    x.BackColor = System.Drawing.Color.LightGoldenrodYellow; // change background color to light golden color
                    AITimer.Stop(); // stop the AI-timer
                    Check2in1(currentPlayer.ToString());
                    break; // breaks the loop (must be a better way to write this...?)
                }
                else
                {
                    AITimer.Stop();
                }
            }
        }

        private void resetGame(object sender, EventArgs e)
        {
            label1.Text = "??"; // change the label text to ??

            foreach (Control x in this.Controls) //initiates a foreach loop, looping through all control elements in the current object (which is class.TicTactoe.Form1)
            {

                if (x is Button && x.Tag == "play") // only the board-buttons have the tag play (reset doesn't)
                {
                    ((Button)x).Enabled = true; // turns buttons back on, in case they were disabled
                    ((Button)x).Text = "?"; // reset the text to '?'
                    ((Button)x).BackColor = default(Color); // change button color back to default color (set where?)
                }

            }
        }

        private void Check2in1 (string XO) // let's rewrite this to change the color of the buttons in the winning row
        {
            // vertical
            if
            (
            button1.Text == XO && button2.Text == XO && button3.Text == XO
            )
            {
                WON(XO, button1, button2, button3);
            }

            else if
            (
            button4.Text == XO && button5.Text == XO && button6.Text == XO
            )
            {
                WON(XO, button4, button5, button6);
            }
                    
            else if
            (
            button7.Text == XO && button8.Text == XO && button9.Text == XO
            )
            {
                WON(XO, button7, button8, button9);
            }

            // horizontal
            else if
            (                        
            button1.Text == XO && button4.Text == XO && button7.Text == XO
            )
            {
                WON(XO, button1, button4, button7);
            }
            else if
            (
            button2.Text == XO && button5.Text == XO && button8.Text == XO
            )
            {
                WON(XO, button2, button5, button8);
            }
            else if
            (
            button3.Text == XO && button6.Text == XO && button9.Text == XO
            )
            {
                WON(XO, button3, button6, button9);
            }
            // diagonal
            else if
            (
            button1.Text == XO && button5.Text == XO && button9.Text == XO
            )
            {
                WON(XO, button1, button5, button9);
            }

            else if
            (
            button3.Text == XO && button5.Text == XO && button7.Text == XO
            )
            {
                WON(XO, button3, button5, button7);
            }
        }

        private void WON(string XO, Control A, Control B, Control C)
        {
            label1.Text = XO + " Wins";
            A.BackColor = System.Drawing.Color.Tomato;
            B.BackColor = System.Drawing.Color.Tomato;
            C.BackColor = System.Drawing.Color.Tomato;
            foreach (Control x in this.Controls)
            {
                
                if (x is Button && x.Tag == "play")
                {
                    ((Button)x).Enabled = false; // disable all buttons - it is no longer possible to play when someone has won
                    

                }

            }
        }
    }
}
